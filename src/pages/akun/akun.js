import React, { useState, useEffect } from 'react';
import { View, Text, StatusBar, Image, TouchableOpacity, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Akun = ({navigation}) => {
  return (
    <View style={{flex: 1}}><StatusBar barStyle={'light-content'}  backgroundColor='#383838'/>
    <View style={{flex: 1, backgroundColor: '#FFFFFF',}}>
    <View style={{flex: 1, alignItems: 'center', marginTop: 120 }}><Text style={{fontWeight: 'bold', fontSize: 50, color: '#383838'}}>My Account</Text></View>
    </View>
    <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
    <View style={{justifyContent: 'center',alignItems: 'center'}}>
      <Image source={require('../../assets/img/bg.jpg')}
      style={{width: 220, height: 220, borderRadius: 220 / 2, position: 'absolute'}}/>
      </View>
      <View>
        <Text style={{fontWeight: 'bold', fontSize: 18, textAlign: 'center', marginTop: 60, color: '#FFFFFF'}}>Me</Text>
        <Text style={{textAlign: 'center', color: '#FFFFFF'}}>That's Right</Text>
        <View>
          <TouchableOpacity onPress={() => navigation.navigate('Home')} style={{backgroundColor: '#3880ff', paddingVertical: 10, marginTop: 30, marginHorizontal: 150, borderRadius: 15,}}>
            <Text style={{color: '#FFFFFF', textAlign: 'center', fontSize: 20}}>Login</Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity onPress={() => Alert.alert('Penting','Tekan Tombol Kembali Anda.')} style={{backgroundColor: 'orange', paddingVertical: 10, marginTop: 20, marginHorizontal: 150, borderRadius: 15,}}>
            <Text style={{color: '#FFFFFF', textAlign: 'center', fontSize: 20}}>Log-Out</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
    </View>
  );
};

export default Akun;

// import React, { Component, useEffect, useState } from 'react'
// import { View, Text, StatusBar } from 'react-native';
// class Akun extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {  };
//     }
//     render() {
//         return (
//             <View style={{flex: 1}}>
//             <StatusBar barStyle={'light-content'} backgroundColor='#3880ff'/> 
//                 <View style={{flex: 0,7 backgroundColor: '#3880ff'}}></View>
//             </View>
//         );
//     }
// }

// export default Akun;



// import { StyleSheet, Text, View, Image, TouchableOpacity, ImageBackground } from 'react-native'
// import {Bg} from '../../assets'
// import React from 'react'

// const Akun = () => {
//   return (
//     <View style={styles.page}>
//       <View>
//         <Text style={styles.text}>Akun</Text>
//         <ImageBackground source={Bg} style={styles.img}></ImageBackground>
//       </View>
//     </View>
//   )
// }

// export default Akun

// const styles = StyleSheet.create({
//   page: {
//     flex: 1,
//   },
//   text: {
//     color: 'black'
//   },
//   img: {
//     widht: 50,
//     height: 50
//   }
// })