import { Dimensions, ImageBackground, StyleSheet, Text, View, Image, ScrollView, } from 'react-native'
import React from 'react'
import { ImageHeader, Logo } from '../../assets'
import {WARNA_ABU_ABU, WARNA_WARNING, WARNA_UTAMA} from '../../utils/index';
//import { WARNA_ABU_ABU } from '../../utils/index';
//import {ScrollView} from 'react-native-gesture-handler';
import { Saldo } from '../../components'
import { Colors } from 'react-native/Libraries/NewAppScreen'
import {ButtonIcon, Pesananaktif} from '../../components';

const Home = () => {
  return (
    <View style={styles.page}>
    <ScrollView showsVerticalScrollIndicator={false}>
      <ImageBackground source={ImageHeader} style={styles.header}>
        <Image source={Logo} style={styles.logo}/>
        <View style={styles.hello}>
          <Text style={styles.selamat}>Selamat Datang, </Text>
          <Text style={styles.username}>Alhazby</Text>
        </View>
      </ImageBackground>
      <Saldo/>
      <View style={styles.layanan}>
        <Text style={styles.label}>Layana Kami</Text>
       <View style={styles.iconlayanan}>
        <ButtonIcon title='Kiloan' type='layanan'/>
        <ButtonIcon title='Satuan' type='layanan'/>
        <ButtonIcon title='VIP' type='layanan'/>
        <ButtonIcon title='Karpet' type='layanan'/>
        <ButtonIcon title='Setrika Saja' type='layanan'/>
        <ButtonIcon title='Ekspress' type='layanan'/>
       </View>
      </View>
      <View style={styles.pesananaktif}>
      <Text style={styles.label}>Status Pesanan</Text>
      <Pesananaktif title='Pesanan No. 0002141' status='Sudah Selesai'/>
      <Pesananaktif title='Pesanan No. 0002141' status='Dalam Proses'/>
      <Pesananaktif title='Pesanan No. 0002141' status='Sudah Selesai'/>
      <Pesananaktif title='Pesanan No. 0002141' status='Dalam Proses'/>
      </View>
    </ScrollView>
    </View>
  )
}

export default Home

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    width: windowWidth,
    height: windowHeight*0.30,
    paddingHorozontal: 30,
    paddingTop: 10
  },
  logo: {
    width: windowWidth*0.30,
    height: windowHeight*0.06
  },
  hello: {
    marginTop: windowHeight*0.03,
    paddingLeft: 15
  },
  selamat: {
    fontSize: 24,
    fontFamily: 'TitilliumWeb-Regular',
    color:"#383838"
  },
  username: {
    fontSize: 22,
    fontFamily: 'TitilliumWeb-Bold',
    color:"#383838"
  },
  layanan: {
    paddingLeft: 30,
    paddingTop: 15,
  },
  label: {
    fontSize: 18,
    fontFamily: 'TitilliumWeb-Bold',
    color:"#383838"
  },
  iconlayanan: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    flexWrap: 'wrap',
  },
  pesananaktif: {
    paddingTop: 10,
    paddingHorizontal: 30,
    backgroundColor: WARNA_ABU_ABU,
    flex: 1,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  }
})