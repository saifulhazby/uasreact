import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { WARNA_ABU_ABU } from '../../utils'
import {ButtonIcon, Pesananaktif} from '../../components';

const Pesanan = () => {
  return (
    <View style={styles.page}>
      <ScrollView>
        <View style={styles.status}>
          <Text style={styles.label}>Status Pesanan</Text>
          <Pesananaktif title='Pesanan No. 0002141' status='Sudah Selesai'/>
          <Pesananaktif title='Pesanan No. 0002142' status='Dalam Proses'/>
          <Pesananaktif title='Pesanan No. 0002143' status='Sudah Selesai'/>
          <Pesananaktif title='Pesanan No. 0002144' status='Dalam Proses'/>
          <Pesananaktif title='Pesanan No. 0002145' status='Sudah Selesai'/>
          <Pesananaktif title='Pesanan No. 0002146' status='Dalam Proses'/>
          <Pesananaktif title='Pesanan No. 0002147' status='Sudah Selesai'/>
          <Pesananaktif title='Pesanan No. 0002148' status='Dalam Proses'/>
          <Pesananaktif title='Pesanan No. 0002149' status='Sudah Selesai'/>
          <Pesananaktif title='Pesanan No. 0002140' status='Dalam Proses'/>
          <Pesananaktif title='Pesanan No. 0002241' status='Sudah Selesai'/>
          <Pesananaktif title='Pesanan No. 0001341' status='Dalam Proses'/>
          <Pesananaktif title='Pesanan No. 0002151' status='Sudah Selesai'/>
          <Pesananaktif title='Pesanan No. 0002251' status='Dalam Proses'/>
          <Pesananaktif title='Pesanan No. 0002141' status='Sudah Selesai'/>
          <Pesananaktif title='Pesanan No. 0002841' status='Dalam Proses'/>
          <Pesananaktif title='Pesanan No. 0002041' status='Sudah Selesai'/>
          <Pesananaktif title='Pesanan No. 0007641' status='Dalam Proses'/>
        </View>
      </ScrollView>
    </View>
  )
}

export default Pesanan

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  status: {
    paddingTop: 10,
    paddingHorizontal: 25,
    backgroundColor: WARNA_ABU_ABU,
    flex: 1,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
  label: {
    fontSize: 18,
    fontFamily: 'TitilliumWeb-Bold',
    color:"#383838"
  },
})