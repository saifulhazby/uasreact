import { ImageBackground, StyleSheet, Text, View, Image } from 'react-native'
import React, { useEffect } from 'react'
import { Sb, Logo } from '../../assets'
//import { StackActions } from '@react-navigation/native'

const Splash = ({ navigation }) => {
 useEffect(() => {
   setTimeout( () => {
     navigation.replace('MainApp');
  }, 1000)
 }, [navigation]);
  
  return (
    <ImageBackground source={Sb} style={styles.background}>
      <Image source={Logo} style={styles.logo}/>
    </ImageBackground>
  )
}

export default Splash;

const styles = StyleSheet.create({
  background: {
      flex: 1,
      alignItem: 'center',
      justifyContent: 'center'
  },
  logo: {
    width: 222,
    height: 105
}
})