import Home from "./Home/home"
import Pesanan from "./pesanan/index"
import Akun from "./akun/akun"
import Splash from "./splash/index"

export { Splash, Pesanan, Akun, Home }