import { StyleSheet, Text, View, Dimensions } from 'react-native'
import React from 'react'
import {WARNA_UTAMA} from '../../utils/index';
import ButtonIcon from '../buttonicon';
import Gap from '../gap';

const Saldo = () => {
  return (
    <View style={styles.container}>
      <View style={styles.informasisaldo}>
        <View style={styles.text}>
          <Text style={styles.labelsaldo}>Saldo :</Text>
          <Text style={styles.valuesaldo}>Rp. 1.000.000</Text>
        </View>
        <View style={styles.text}>
          <Text style={styles.labelpoint}>Antar Point :</Text>
          <Text style={styles.valuepoint}>100 Points</Text>
        </View>
      </View>
      <View style={styles.buttonaksi}>
        <ButtonIcon title='Add Saldo'/>
        <Gap width={10}/>
        <ButtonIcon title='Get Point'/>
      </View>
    </View>
  )
}

export default Saldo

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 17,
    marginHorizontal: 30,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 3,
    },
    shadowOpacity: 0.39,
    shadowRadius: 4.65,

    elevation: 7,
    marginTop: -windowHeight*0.05,
    flexDirection: 'row'
  },
  text: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    color:"#383838",
  },
  informasiSaldo: {
    width: '60%',
  },
  labelsaldo: {
    fontSize: 20,
    fontFamily: 'TitilliumWeb-Regular',
    color:'#383838',
   fontWeight:'bold'
  },
  valuesaldo: {
    fontSize: 20,
    fontFamily: 'TitilliumWeb-Bold',
    color:'#383838',
    paddingLeft: 7
  },
  labelpoint: {
    fontSize: 12,
    fontFamily: 'TitilliumWeb-Regular',
    paddingTop: 9,
    color: '#383838',
  },
  valuepoint: {
    fontSize: 12,
    fontFamily: 'TitilliumWeb-Bold',
    paddingTop: 9,
    color: WARNA_UTAMA,
  },
  buttonaksi: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
})